//
//  BuyViewController.swift
//  LazyZalandoApp
//
//  Created by iosdev on 2.12.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class BuyViewController: UIViewController {
    
    var incomingData : ItemElement?

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var ImageView: UIImageView!
    
    var imagearray = [UIImage]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0..<(incomingData?.item.images.count)! {
            imagearray.append(UIImage(data: (incomingData?.item.images[i].imageData)!)!)
        }
        
        // Do any additional setup after loading the view.
        
        print(type(of: imagearray))
        priceLabel.text = String("Price  \(incomingData!.item.price.amount) eur")
        brandLabel.text = String("Brand \(incomingData!.item.brand) ")
        sizeLabel.text  = String("Size \(incomingData!.item.size) ")
        owner.text = String("Seller: \(incomingData!.user.profileName)")
       // ImageView.image = UIImage(data: (incomingData?.item.images[0].imageData)!)
        print(imagearray)
        ImageView.animationImages =  imagearray
        ImageView.animationDuration = 2
        ImageView.startAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
