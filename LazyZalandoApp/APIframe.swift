//
//  APIframe.swift
//  LazyZalandoApp
//
//  Created by iosdev on 19.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let title: [Title]
    let items: [ItemElement]
    let allowedFilterTypes: [String]
    let totalCount: TotalCount

    enum CodingKeys: String, CodingKey {
        case title, items
        case allowedFilterTypes = "allowed_filter_types"
        case totalCount = "total_count"
    }
}

// MARK: - ItemElement
struct ItemElement: Codable {
    let item: ItemItem
    let user: User
}

// MARK: - ItemItem
struct ItemItem: Codable {
    let id, brand, category, size: String
    let price: Price
    let images: [Image]
    let isMine, isWebshop: Bool

    enum CodingKeys: String, CodingKey {
        case id, brand, category, size, price, images
        case isMine = "is_mine"
        case isWebshop = "is_webshop"
    }
}

// MARK: - Image
struct Image: Codable {
    let id: String
    let url: String
    
    var imageData: Data {
        
            let imageuURL = URL(string: url)
            let image = try! Data(contentsOf: imageuURL!)
        return image
    }
}

// MARK: - Price
struct Price: Codable {
    let amount: Double
    let currency: String
}

// MARK: - User
struct User: Codable {
    let userID, profileName: String
    let profileImageURL: String
    let isVerified: Bool

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case profileName = "profile_name"
        case profileImageURL = "profile_image_url"
        case isVerified = "is_verified"
    }
}

// MARK: - Title
struct Title: Codable {
    let lang, value: String
}

// MARK: - TotalCount
struct TotalCount: Codable {
    let value: Int
    let relation: String
}

