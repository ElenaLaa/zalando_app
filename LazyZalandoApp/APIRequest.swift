//
//  APIRequest.swift
//  LazyZalandoApp
//
//  Created by iosdev on 19.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation

protocol APIRequestDelegate {
    func didUpdateData(welcome: Welcome)
}

struct APIRequest {
    var delegate: APIRequestDelegate?
    
   // var resourceURL:URL
    let header = [
        "x-wardrobe-apikey": "9hC4HxmqRe7NG3HDPg9c6WEj",
        "Content-Type": "Application/json"
    ]
    // This shud come from screen 3
    
    mutating func loadData(body: Dictionary<String,Array<String>>){
        //func loadData(category: int,color: int,size: int)
        
        /*
         var  body = [
         "categories": ["1"]
         //TODO get info from call
         ]*/
        let resourceString = "https://api.zalando-wardrobe.de/api/marketplace/groups/webshop/all_items/search?page_size=10&page_number=1&sort=created_at&sort_direction=desc&b2c=true&c2c=true"
       // print(type(of: body))
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
       // self.resourceURL = resourceURL
        var request = URLRequest(url: resourceURL)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = header
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: body, options: [])
            performRequest(request: request)
        } catch let err {
            print(err)
        }
    }
    
    
    
    func performRequest(request: URLRequest)  {
        
        //get the data from Zalando api
       
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print("Somthing went wrong: \(error)")
                return
            }
            if let safedata = data {
                    if let muutatama = self.parseJson(welcomeData: safedata){
                        self.delegate?.didUpdateData( welcome: muutatama)
                }
            }
            // var json: Welcome?
           
            
            //TODO iterate through items and iterate through images +
            // fetch images they need api key
            
        }
        dataTask.resume()
    }


func parseJson(welcomeData: Data) -> Welcome?{
    let decoder = JSONDecoder()
    do {
                   let json = try decoder.decode(Welcome.self, from: welcomeData) // decode the data from Zalando api
                   
                   let items = json.items.map{value -> ItemElement in
                       return value
                   }
                    let title = json.title.map{value -> Title in
                        return value
                    }
                   let allowed = json.allowedFilterTypes.map{value -> String in
                       return value
                   }
                    let total = json.totalCount
        
        
                  // return 1
                   return Welcome(title: title, items: items, allowedFilterTypes: allowed, totalCount: total)
               }catch {
                   print(error)
                return nil
               }
}
}
