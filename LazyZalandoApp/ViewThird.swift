//
//  ViewThird.swift
//  LazyZalandoApp
//
//  Created by iosdev on 19.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import Foundation
import UIKit

class ViewThird: ViewController {
    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    let categories = ["Clothes", "Footwear", "Accessories", "Underwear", "Design", "New"]
    
    let categoriesImages: [UIImage] = [
        UIImage (named: "clothes")!,
        UIImage (named: "footwear")!,
        UIImage (named: "accessories")!,
        UIImage (named: "underwear")!,
        UIImage (named: "design")!,
        UIImage (named: "new")!
    ]
    
    @IBAction func tapAction(_ sender: Any) {
        print("WHEE")
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let destinationViewController = mainStoryBoard.instantiateViewController(withIdentifier: "FourthViewController") as? FourthViewController else {
            print("no destinatio")
            return
        }
        print(navigationController)
         navigationController?.pushViewController(destinationViewController, animated: false)
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        let layout = self.categoryCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets (top: 0, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: (self.categoryCollectionView.frame.size.width - 20)/2, height: self.categoryCollectionView.frame.size.height/3)
        
    }
}

extension ViewThird: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCell
        cell.categoryLabel.text = categories[indexPath.item]
        cell.categoryImage.image = categoriesImages[indexPath.item]
        cell.layer.backgroundColor = UIColor.orange.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.orange.cgColor
        
        return cell
    }
    
    func  collectionView(_ collectionView: UICollectionView, didSelectItemAt  indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.red.cgColor
        cell?.layer.borderWidth = 3
        
    }
    func  collectionView(_ collectionView: UICollectionView, didDeselectItemAt  indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.orange.cgColor
        cell?.layer.borderWidth = 0.5
        
    }
}








