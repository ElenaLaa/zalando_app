//
//  FourthViewController.swift
//  LazyZalandoApp
//
//  Created by iosdev on 25.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, APIRequestDelegate {
    
    @IBOutlet weak var allPicker: UIPickerView!
    
    func didUpdateData(welcome: Welcome) {
        image.removeAll()
        item.removeAll()
        welcome.items.map{value -> ItemElement in
            image.append(value.item.images[0].imageData)
            item.append(value)
            return value
        }
        items.append(item)
        images.append(image)
        index += 1
        print(images)
        if index == 3 {
           
        DispatchQueue.main.async {
            print(type(of: welcome))
            let hatPick = self.allPicker
            hatPick!.delegate = self
            hatPick!.dataSource = self
            hatPick!.transform = CGAffineTransform(rotationAngle: -90 * (.pi/180))
               
               // hatPick.center = self.view.center
            hatPick!.frame = CGRect(x: 150, y: 350, width: 200, height:  200 )
            hatPick!.layer.borderColor = UIColor.black.cgColor
               
            hatPick!.layer.borderWidth = 1.5
            self.view.addSubview(hatPick!)
            }}
    /*    let images = welcome.items.map{value -> UIImage in
            let imageURL = URL(string: value.item.images[0].url)!
            let imageData = try! Data(contentsOf: imageURL)
            let images = UIImage(data: imageData)!
            image.append(images)
            
            return images}
        print(images.count)

      //  let imageURL = URL(string: imageURLstring)!
        
        //    UIImage(named: imageURLstring)
        //image.append(contentsOf: images)*/
       
    }
    
    var selectedRow:Int = 0
    var index = 0
    var item = [ItemElement]()
    var items = [[ItemElement]]()
    var images = [[Data]]()
    var image = [Data]()
    let height:CGFloat = 100
    let width:CGFloat = 100
    
   
    
    var apirequest = APIRequest()
 //   let hatPick = UIPickerView()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "selection1"){
            (segue.destination as! BuyViewController).incomingData = items[2][selectedRow]
        }
        if (segue.identifier == "selection2"){
            (segue.destination as! BuyViewController).incomingData = items[1][selectedRow]
        }
        if (segue.identifier == "selection3"){
            (segue.destination as! BuyViewController).incomingData = items[0][selectedRow]
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apirequest.delegate = self
        DispatchQueue.global().sync {
            Load(category: 2)
            print("1")
        }
        
        Load(category: 5)
        Load(category: 8)
        print("tama 2")
    /*    hatPick.delegate = self
        hatPick.dataSource = self
        hatPick.transform = CGAffineTransform(rotationAngle: -90 * (.pi/180))
        
        // hatPick.center = self.view.center
        hatPick.frame = CGRect(x: 150, y: 350, width: 150, height:  50 )
        hatPick.layer.borderColor = UIColor.black.cgColor
        
        hatPick.layer.borderWidth = 1.5
        self.view.addSubview(hatPick)*/
    }
    
   
    
    func Load(category: Int)  {
       
            
            self.apirequest.loadData(body: ["categories":["\(category)"]])
        
        
        // let apirequest = APIRequest(body: ["categories":["\(category)"]])
        /* apirequest.loadData { [weak self] result in
         switch result {
         case.failure(let err):
         print(err)
         case .success(let welcome):
         var url:URL?
         //  let url = URL(string: (welcome[0].item.images[0].url))!
         for  item in 0..<welcome.count {
         url = URL(string: (welcome[item].item.images[0].url))!
         }
         
         self?.listOfItems = welcome
         
         
         
         
         if let data = try? Data(contentsOf: url!) {
         DispatchQueue.main.async {
         self!.pic =  data
         print(url)
         }
         }
         }*/
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 3
       }
       
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return image.count
       }
       func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
           return height
       }
       func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
           return width
       }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
                    let myView = UIView(frame: CGRect(x: 0, y: 0, width:100, height: 150))
                    var myImageView = UIImageView(frame:  CGRect(x:0,y:0,width: 100,height:  100))
              //   print("tama 1",welcome)
        
             myImageView.image = UIImage(data: images[component][row] )
                    myView.addSubview(myImageView)
                    myView.transform = CGAffineTransform(rotationAngle: 90 * (.pi/180))
                    return myView
                    
                }
       
      
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           selectedRow = row
       
        print(component, row)
       }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
/*extension UIImage {
    // Loads an image from the specified URL on a background thread
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data){
                    self?.image = image
                }
        }
        
    }
    
}
}*/
