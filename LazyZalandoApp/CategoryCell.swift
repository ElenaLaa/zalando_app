//
//  CategoryCell.swift
//  LazyZalandoApp
//
//  Created by iosdev on 24.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
}
